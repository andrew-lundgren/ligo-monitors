#!/bin/bash
#
# Run the gwdetchar.overflow module on a daily stride

export MPLCONFIGDIR=${HOME}/.matplotlib
. ~/.bash_profile
if [[ "$(whoami)" == "detchar" ]]; then
    unset X509_USER_PROXY
fi
set -e

# set bash environment
here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# set python environment
activate_conda_environment
echo "-- Environment set"
echo "Conda prefix: ${CONDA_PREFIX}"

# get run date
gpsstart=`gps_start_yesterday`
gpsend=`gps_start_today`
date_=`yyyymmdd ${gpsstart}`
duration=$((gpsend-gpsstart))
echo "-- Identified run date as ${date_}"

# set up output directory
htmlbase=${HOME}/public_html/overflows/
outdir=${htmlbase}/day/${date_}
mkdir -p ${outdir}
cd ${outdir}
echo "-- Moved to output directory: `pwd`"

# get IFO
if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi
set +e

# get FECs and map
fecs="8 10 19 29 30 88 97 98"
if [[ "${IFO}" == "H1" ]]; then
    FECMAP="--fec-map https://lhocds.ligo-wa.caltech.edu/exports/detchar/fec/"
elif [[ "${IFO}" == "L1" ]]; then
    FECMAP="--fec-map https://llocds.ligo-la.caltech.edu/data/detchar/fec/"
fi

# get analysis flag
FLAG="${IFO}:DMT-GRD_ISC_LOCK_NOMINAL:1"

# run the code
outhdf=${IFO}-OVERFLOWS-$gpsstart-$duration.h5

nagios_status 0 "Daily overflows analysis for ${date_} is running" 10000 "Daily overflows analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json

LOGFILE="/tmp/daily-overflows-$(whoami).err"
cmd="python -m gwdetchar.overflow $gpsstart $gpsend $fecs --ifo ${IFO} --frametype ${IFO}_R --state-flag ${FLAG} --output-file $outhdf -I --html index.html --plot --deep --nproc 4 ${FECMAP}"
echo "$ $cmd" && eval $cmd 2> ${LOGFILE}

EXITCODE="$?"

# write JSON output
TIMER=100000
TIMEOUT="Daily overflows analysis has not run since ${date_}"
if [ "$EXITCODE" -eq 0 ]; then
    MESSAGE="Daily overflows analysis for ${date_} complete"
    nexit=0
else
    MESSAGE=`echo -n "Daily overflows analysis for ${date_} failed with exitcode $EXITCODE\\\n" && sed ':a;N;$!ba;s/\n/\\\n/g' ${LOGFILE} | tr '"' "'"`
    nexit=2
fi
nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
exit ${EXITCODE}
