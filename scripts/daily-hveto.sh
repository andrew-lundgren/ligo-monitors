#!/bin/bash
#
# Run the hveto module on a daily cadence
#

# -- set up -------------------------------------------------------------------

. ~/.bash_profile
if [[ "$(whoami)" == "detchar" ]]; then
    unset X509_USER_PROXY
fi
set -e

# set bash environment
here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# set python environment
activate_conda_environment
echo "-- Environment set"
echo "Conda prefix: ${CONDA_PREFIX}"

# -- set condor accounting information ----------------------------------------
# these data should match the parameters in the /condor/daily-hveto.sub file
#
# these are picked up by gwdetchar.omega.batch for the omega scans

if [[ "$(whoami)" == "detchar" ]]; then
    export _CONDOR_ACCOUNTING_GROUP="ligo.prod.o4.detchar.dqtriggers.hveto"
    export _CONDOR_ACCOUNTING_USER="joshua.smith"
else
    export _CONDOR_ACCOUNTING_GROUP="ligo.dev.o4.detchar.dqtriggers.hveto"
    export _CONDOR_ACCOUNTING_USER="$(whoami)"
fi

# -- get run times ------------------------------------------------------------
# logic goes like this
#     - if we're less than 8 hours into a day, presume we want to process
#       all of yesterday
#     - if we're 8 < us < 16 hours into a day, process the first 8 hrs of today
#     - if we're 16 < us < 24 hours into a day, process the first 16 hrs

echo "-- Determining run parameters"
if [ "$#" -ne 0 ]; then  # user specified date
    nagios=0  # don't write nagios JSON files
    date_=$1
    year=${date_:0:4}
    month=${date_:4:2}
    day=${date_:6:2}
    gpsstart=`python -m gwpy.time ${year}-${month}-${day}`
    duration=86400
    gpsend=$((gpsstart+duration))
else
    nagios=1  # write nagios JSON files
    now=`python -m gwpy.time now`
    startofday=`gps_start_today`
    elapsed=$((now-startofday))
    echo "Now: $now"
    echo "Today: $startofday"
    echo "Elapsed: $elapsed"
    if [[ "$elapsed" -lt 28800 ]]; then
        gpsstart=`gps_start_yesterday`
        gpsend=$startofday
        duration=$((gpsend-gpsstart))
    elif [[ "$elapsed" -lt 57600 ]]; then
        gpsstart=$startofday
        duration=28800
        gpsend=$((gpsstart+duration))
    else
        gpsstart=$startofday
        duration=57600
        gpsend=$((gpsstart+duration))
    fi
    date_=`yyyymmdd $gpsstart`
fi

echo "Identified run date as ${date_}"
echo "Will process $gpsstart-$gpsend"
echo "Duration: $duration"

# -- create directories -------------------------------------------------------

htmlbase=${HOME}/public_html/hveto/

# create day dir
daydir=${htmlbase}/day/${date_}
mkdir -p $daydir
cd $daydir

# create dir for this run
gpstag="$gpsstart-$gpsend"
mkdir -p $gpstag
outdir=${htmlbase}/day/${date_}/$gpsstart-$gpsend
mkdir -p ${outdir}

cd $gpstag

echo "-- Moved to output directory: `pwd`"

# get IFO and FECs
if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi
set +e

# -- run ----------------------------------------------------------------------

# set hveto parameters
configuration="${here_}/../configurations/hveto/h1l1-hveto-daily-o4.ini"
ncpu=4

# run the code
[[ $nagios -ne 0 ]] && nagios_status 0 "Daily Hveto analysis for ${date_} is running" 14400 "Daily Hveto analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json

LOGFILE="/tmp/daily-hveto-$(whoami).err"
echo "-- Running hveto | logging goes to ${LOGFILE}"
cmd="python -m hveto $gpsstart $gpsend --ifo ${IFO} --config-file $configuration --nproc $ncpu --omega-scans 5"
echo "$ $cmd" && eval $cmd 2> ${LOGFILE}

EXITCODE="$?"
echo "-- Hveto has exited with code $EXITCODE"

# -- post-process -------------------------------------------------------------

# link this run as 'latest'
latest="latest"
cd $daydir
[[ -L $latest ]] && unlink $latest
ln -s $gpstag $latest

# write JSON output
if [[ $nagios -ne 0 ]]; then
    TIMER=104400
    TIMEOUT="Daily Hveto analysis has not run since ${date_}"
    if [ "$EXITCODE" -ne 0 ]; then
        MESSAGE="Daily Hveto analysis for ${date_} failed with exitcode $EXITCODE\\\nSee log file under ${LOGFILE} on `hostname -f`"
        nexit=2
    else
        MESSAGE="Daily Hveto analysis for ${date_} complete"
        nexit=0
    fi
    nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
fi

# exit
exit ${EXITCODE}
